Ce module ajoute à WinDev des fonctionnalités pour mieux gérer les types primitifs et avancés du W-Langage.

Les types disponibles sont les suivants : 

- cVariant : Cette classe permet de gérer les types variants et membre variant de WinDev.
